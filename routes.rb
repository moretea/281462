ActionController::Routing::Routes.draw do |map|
  map_i18n map do |m|
    map.connect blah
    map.resource meh
  end
end

# helper
def map_i18n map, &block
  if site_has_multiple_languages
    map.with_options :path_prefix => '/:locale' do |localized_map|
      localized_map.instance_exec(block)  
    end
  else
    map.instance_exec(block)      
  end
end